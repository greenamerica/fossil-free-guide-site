/* Author: 

*/



$(document).ready(function(){
	$('body').removeClass('no-js');
/* Scrolling */
	$('nav, article').localScroll({
	   target:'body',
	   offset: {top: -48},
	   easing: 'swing'
	},500);

  $('article div:not(.background) img').lazyload({
    threshold : 200,
    effect : "fadeIn"
  });

});

/* Only < 50em */
if (Modernizr.mq('only screen and (max-width: 800px)')) {

	$('nav[role=navigation]').prepend('<div id="menu-icon">Table of Contents</div>');
	$('nav[role=navigation] > ol').slideToggle();
	/* toggle nav */
	$("#menu-icon").on("click", function(){
		$("nav[role=navigation] > ol").slideToggle();
		$(this).toggleClass("active");
	});


}

/* Anything > 50em */
if (Modernizr.mq('only screen and (min-width: 800px)')) {



if ($('#home').length > 0) {
  positionTOC();
  navActive();
}

  $('.post-header h1').fitText(1.75);
  
  $('article .background img').lazyload({
    threshold : 400,
    effect : "fadeIn"
  });
}

/* Between 48em and 60em */
if (Modernizr.mq('only screen and (min-width: 768px) and (max-width: 960px)')) {

}

/* Anything > 60em */
if (Modernizr.mq('only screen and min-width: 960px)')) {

}


/* High pixel density */
if (window.devicePixelRatio >= 1.5) {

}

/* Portrait orientation */
if (window.orientation == 0 || 180) {

}

/* Landscape orientation */
if (window.orientation == 90 || 240) {

}