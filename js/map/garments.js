var basic_choropleth = new Datamap({
  element: document.getElementById("map"),
  fills: {
    defaultFill: "#cccccc",
    problem: "#ab619a"
  },
  data: {
    BOL: { fillKey: "problem" },
    ARG: { fillKey: "problem" },
    IND: { fillKey: "problem" },
    THA: { fillKey: "problem" },
    LAO: { fillKey: "problem" },
    KHM: { fillKey: "problem" },
    VNM: { fillKey: "problem" },
    BGD: { fillKey: "problem" }
  },
  geographyConfig: {
    borderWidth: 0
  }
});