var basic_choropleth = new Datamap({
  element: document.getElementById("map"),
  fills: {
    defaultFill: "#cccccc",
    problem: "#ab619a"
  },
  data: {
    CHN: { fillKey: "problem" },
  },
  geographyConfig: {
    borderWidth: 0
  }
});