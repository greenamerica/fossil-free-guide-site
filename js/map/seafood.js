var basic_choropleth = new Datamap({
  element: document.getElementById("map"),
  fills: {
    defaultFill: "#cccccc",
    problem: "#ab619a"
  },
  data: {
    MMR: { fillKey: "problem" },
    THA: { fillKey: "problem" },
    KHM: { fillKey: "problem" }
  },
  geographyConfig: {
    borderWidth: 0
  }
});