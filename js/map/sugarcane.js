var basic_choropleth = new Datamap({
  element: document.getElementById("map"),
  fills: {
    defaultFill: "#cccccc",
    problem: "#ab619a"
  },
  data: {
    NIC: { fillKey: "problem" }, 
    CRI: { fillKey: "problem" },
    SLV: { fillKey: "problem" },
    GTM: { fillKey: "problem" },
    MMR: { fillKey: "problem" },
    BOL: { fillKey: "problem" },
  },
  geographyConfig: {
    borderWidth: 0
  }
});