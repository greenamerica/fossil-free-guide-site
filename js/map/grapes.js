var basic_choropleth = new Datamap({
  element: document.getElementById("map"),
  fills: {
    defaultFill: "#cccccc",
    problem: "#ab619a"
  },
  data: {
    USA: { fillKey: "problem" },
    ZAF: { fillKey: "problem" }
  },
  geographyConfig: {
    borderWidth: 0
  }
});